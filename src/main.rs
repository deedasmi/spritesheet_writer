use clap::{value_t, App, Arg};
use log::{debug, info};
use serde::Serialize;
use std::fs::{File};
use std::io::Write;
use std::path::Path;

#[derive(Serialize, Debug)]
pub struct SpriteSheet {
    texture_width: u64,
    texture_height: u64,
    sprites: Vec<Sprite>,
}

impl SpriteSheet {
    fn new(w: u64, h: u64) -> Self {
        SpriteSheet {
            texture_width: w,
            texture_height: h,
            sprites: Vec::new(),
        }
    }
    fn insert(&mut self, s: Sprite) {
        self.sprites.push(s)
    }
    pub fn build(&mut self, sf: &SpriteFactory, sx: u64, sy: u64, rows: u64, cols: u64, sw: u64, sh: u64) {
        for r in 0..rows {
            let cy = sy + (r * sh);
            for c in 0..cols {
                let cx = sx + (c * sw);
                self.insert(sf.produce(cx, cy))
            }
        }
    }
    pub fn serialize(self) -> String {
        let pc = ron::ser::PrettyConfig::default();
        ron::ser::to_string_pretty(&self, pc).expect("Failed to serialize")
    }
}

fn write_out<T: AsRef<Path>>(bytes: &[u8], f: T) -> std::io::Result<()> {
    let mut file = File::create(f).unwrap();
    file.write_all(bytes)
}

#[derive(Serialize, Debug)]
pub struct Sprite {
    x: u64,
    y: u64,
    width: u64,
    height: u64,
}

pub struct SpriteFactory {
    width: u64,
    height: u64,
}

impl SpriteFactory {
    pub fn new(width: u64, height: u64) -> Self {
        SpriteFactory { width, height }
    }
    pub fn produce(&self, x: u64, y: u64) -> Sprite {
        Sprite {
            x,
            y,
            width: self.width,
            height: self.height,
        }
    }
}

fn main() {
    simple_logger::init().unwrap();
    let matches = App::new("Sprite Sheet Writer")
        .version("0.1")
        .author("Deedasmi <rap1011@ksu.edu>")
        .about("Writes Sprite information for rectangular spritesheets to RON.")
        .arg(
            Arg::with_name("Output File")
                .help("Select the location to write the file.")
                .required(true)
                .index(1),
        )
        .arg(
            Arg::with_name("Texture Width")
                .help("Full Texture Width")
                .required(true)
                .index(2),
        )
        .arg(
            Arg::with_name("Texture Height")
                .help("Full Texture Height")
                .required(true)
                .index(3),
        )
        .arg(
            Arg::with_name("Start X")
                .help("X value of where to start defining sprites.")
                .required(true)
                .index(4),
        )
        .arg(
            Arg::with_name("Start Y")
                .help("Y value of where to start defining sprites.")
                .required(true)
                .index(5),
        )
        .arg(
            Arg::with_name("Sprite Width")
                .help("Width of each sprite to define")
                .required(true)
                .index(6),
        )
        .arg(
            Arg::with_name("Sprite Height")
                .help("Height of each sprite to define")
                .required(true)
                .index(7),
        )
        .arg(
            Arg::with_name("Rows")
                .help("Number of rows")
                .required(true)
                .index(8),
        )
        .arg(
            Arg::with_name("Columns")
                .help("Number of Columns")
                .required(true)
                .index(9),
        )
        .get_matches();
    let tw = value_t!(matches, "Texture Width", u64).expect("Texture Width must be u64");
    let th = value_t!(matches, "Texture Height", u64).expect("Texture Height must be u64");
    let mut ss = SpriteSheet::new(tw, th);

    let sx = value_t!(matches, "Start X", u64).expect("Start X must be u64");
    let sy = value_t!(matches, "Start Y", u64).expect("Start Y must be u64");
    let sw = value_t!(matches, "Sprite Width", u64).expect("Sprite Width must be u64");
    let sh = value_t!(matches, "Sprite Height", u64).expect("Sprite Height must be u64");
    let rows = value_t!(matches, "Rows", u64).expect("Rows must be u64");
    let cols = value_t!(matches, "Columns", u64).expect("Columns must be u64");
    let sf = SpriteFactory::new(sw, sh);

    ss.build(&sf, sx, sy, rows, cols, sw, sh);

    write_out(ss.serialize().as_bytes(), matches.value_of("Output File").unwrap()).expect("Failed to write file");

    info!("Done!");
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::io::Read;

    #[test]
    fn test() {
        let sf = SpriteFactory::new(128u64, 128u64);
        let mut ss = SpriteSheet::new(4096, 1024);
        ss.build(&sf, 896u64, 0u64, 8u64, 8u64, 128u64, 128u64);
        write_out(ss.serialize().as_bytes(), "target/test.ron").unwrap();
        compare().unwrap()
    }

    fn compare() -> std::io::Result<()> {
        let mut file = File::open("example.ron")?;
        let mut example = String::new();
        file.read_to_string(&mut example)?;

        let mut file = File::open("target/test.ron")?;
        let mut test= String::new();
        file.read_to_string(&mut test)?;
        assert_eq!(example, test);
        Ok(())
    }


}